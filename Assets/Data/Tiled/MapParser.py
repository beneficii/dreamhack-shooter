import json
from pprint import pprint


def parse_layer(layer):
	return {
		'name': layer['name'],
		'width': layer['width'],
		'height': layer['height'],
		'data': ''.join([str(x) for x in layer['data'][::-1]])
	}

def parse_file(name):
	datas = []
	with open(name) as data_file:    
		data = json.load(data_file)


	

	for layer in data['layers']:
		if(layer['name'] == 'Bg'):
			continue
		datas.append(parse_layer(layer))

	return datas


def main():
	datas = parse_file('Levels.json') + parse_file('Parts.json')

	with open('segments.json', 'w') as outfile:
		json.dump(datas, outfile, indent=2)
	#print(json.dumps(datas, indent=2, sort_keys=True))


main()