﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

public class EditorStuff : MonoBehaviour {

    public GameObject BasePrefab;
    public Material GlowMaterial;
    public Material BodyMaterial;

    public string InputPath;
    public string OutputPath;

    [ContextMenu("CreateObjects")]
    void CreateStuff()
    {
        var basicpath = "Assets/Models/";

        DirectoryInfo dir = new DirectoryInfo(basicpath + InputPath);
        foreach (var subdir in dir.GetDirectories())
        {
            Debug.Log("Processing " + subdir.Name + "...");
            GameObject[] parts = new GameObject[2];
            foreach (var item in subdir.GetFiles("*.obj"))
            {
                var objname = basicpath + InputPath + "/" + subdir.Name + "/" + item.Name;
                var mesh = AssetDatabase.LoadAssetAtPath<GameObject>(objname);

                if (mesh.name == "light")
                {
                    parts[0] = mesh;
                } else if (mesh.name == "body")
                {
                    parts[1] = mesh;
                }
            }
            if(parts[0] != null)
                CreateCombinedPrefab(subdir.Name, parts);
        }
    }

    void CreateCombinedPrefab(string pName, GameObject[] parts)
    {
        GameObject instance = Instantiate(BasePrefab, transform);
        instance.name = pName;
        for (int i = 0; i < parts.Length; i++)
        {
            var mesh = Instantiate(parts[i], instance.transform);
            Renderer rend = mesh.GetComponentInChildren<Renderer>();
            /*if (i == 0) // light
            {
                rend.sharedMaterial = GlowMaterial;
            }
            else if (i == 1) // body
            {
                rend.sharedMaterial = BodyMaterial;
            }*/
            rend.sharedMaterial = BodyMaterial;

            mesh.transform.localPosition = Vector3.zero;
            mesh.transform.localScale = Vector3.one * 0.1f;
            mesh.transform.localRotation = Quaternion.Euler(-270, -90, 90);
        }

        // we do this part by hand I guess
        //GameObject prefab = PrefabUtility.CreatePrefab("Assets/Models/" + OutputPath + "/" + pName + ".prefab", BasePrefab);
        //DestroyImmediate(instance);

    }


    [ContextMenu("CreateObjects Free")]
    void CreateStuffFree()
    {
        var basicpath = "Assets/Models/";

        DirectoryInfo dir = new DirectoryInfo(basicpath + InputPath);
        foreach (var subdir in dir.GetDirectories())
        {
            Debug.Log("Processing " + subdir.Name + "...");
            List<GameObject> parts = new List<GameObject>();
            foreach (var item in subdir.GetFiles("*.obj"))
            {
                var objname = basicpath + InputPath + "/" + subdir.Name + "/" + item.Name;
                parts.Add(AssetDatabase.LoadAssetAtPath<GameObject>(objname));
            }
            if (parts[0] != null)
                CreateCombinedPrefab(subdir.Name, parts.ToArray());
        }
    }
}
