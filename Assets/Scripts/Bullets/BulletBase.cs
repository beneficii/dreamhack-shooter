﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBase : MonoBehaviour {

    protected BulletData _data;

    protected Rigidbody2D _rb2D;
    Boundary _boundary;

    private void Awake()
    {
        _rb2D = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        _boundary = new Boundary();
        _boundary.yMax -= 2f;
    }

    public void Init (BulletData data) {
        _data = data;
	}

    public void SetDirection(Vector2 direction)
    {
        _rb2D.velocity = direction * _data.Speed;
    }

    private void Update()
    {
        if(_boundary.OutOfBounds(_rb2D.position))
        {
            Destroy(gameObject);
        }
    }

    protected void Destroy()
    {
        Game.Audio.PlayOneShot(_data.CollisionSound, 0.4f);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Ship"))
        {
            collision.GetComponent<ShipBase>().Health -= _data.Damage;
            Destroy();
        }
    }
}
