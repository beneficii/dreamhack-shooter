﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Addon")]
public class AddonData : ShipData {

    public BulletData bullet;

    public override void OnCollision(ShipPlayer player)
    {
        player.CurrentBullet = bullet;
    }
}
