﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Bullet")]
public class BulletData : ScriptableObject {

    public GameObject prefab;
    public GameObject ObjMesh;

    public float Speed = 15f;
    public int Damage = 1;

    public AudioClip ShootSound;
    public AudioClip CollisionSound;

    public BulletBase Create()
    {
        BulletBase bullet = Instantiate(prefab).GetComponent<BulletBase>();
        bullet.Init(this);
        Instantiate(ObjMesh, bullet.transform);


        if (ShootSound != null)
        {
            Game.Audio.PlayOneShot(ShootSound, 0.3f);
        }

        return bullet;
    }
}
