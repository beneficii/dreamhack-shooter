﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Data/Ship")]
public class ShipData : ScriptableObject
{
    
    public GameObject Mesh;
    public GameObject ExplosionMesh;

    public float Speed;
    public float Firerate;
    public int Health;

    public virtual void OnCollision(ShipPlayer player)
    {
        player.Health--;
    }
 }
