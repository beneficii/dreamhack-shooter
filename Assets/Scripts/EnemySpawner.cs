﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class EnemySpawner : MonoBehaviour {

    public TextAsset SegmentsFile;

    [Header("Ships")]
    public GameObject BasicPrefab;
    public GameObject HorizontalPrefab;
    public GameObject BossPrefab;
    public GameObject BossSmallPrefab;
    public ShipData[] datas;

    Dictionary<string, SegmentData> _segments;

    Boundary _boundary;
    float _nextSpawn = 0f;

    private void Awake()
    {
        _segments = new Dictionary<string, SegmentData>();

        JSONObject json = new JSONObject(SegmentsFile.text);

        foreach (var item in json.list)
        {
            SegmentData segment = JsonUtility.FromJson<SegmentData>(item.Print());
            _segments.Add(segment.name, segment);
        }

    }

    void Start() {
        _boundary = new Boundary(2f);
        StartCoroutine(WaveSpawnCycle());
    }

    ShipBase SpawnEnemy(GameObject shipPrefab, ShipData data, float x, float y = 10)
    {
        ShipBase ship = Instantiate(shipPrefab, new Vector3(x, y), Quaternion.identity)
            .GetComponent<ShipBase>();
        ship.Init(data);

        return ship;
    }

    ShipBase SpawnBoss(GameObject shipPrefab, ShipData data, float x, float y)
    {
        ShipBase ship = SpawnEnemy(shipPrefab, data, x, y);
        ship.GetComponent<PilotBoss>().Init(new Vector2(x,y));
        return ship;
    }

    private void Update()
    {
        if (_nextSpawn <= Time.time)
        {
            //_nextSpawn = Time.time + Random.Range(0.4f, 1.5f);
            //SpawnEnemy();
        }
    }

    IEnumerator WaveSpawnCycle()
    {
        yield return WaveAndCompleete(WaveLevelAndBoss("Level1", datas[3]));
        yield return WaveAndCompleete(RandomWaves(new string[] { "1" }));
        if (Game.Combo.CurrentRank > 3)
        {
            yield return SingleBoss(datas[4]);
        }
        else
        {
            yield return SingleBoss(datas[7]);
        }
        yield return WaveAndCompleete(RandomWaves(new string[] { "1", "2" }));
        yield return WaveAndCompleete(BossWaveSpawnRoutine(_segments["BossSegment1"]), false);
        yield return WaveAndCompleete(RandomWaves(new string[] { "2", "3" }));

        yield return WaveAndCompleete(BossWaveSpawnRoutine(_segments["BossSegment2"]), false);


        yield return new WaitForSeconds(2f);
        PausePanel.Show(PauseState.Victory);
    }

    IEnumerator WaveAndCompleete(IEnumerator wave, bool rerank = true)
    {
        yield return wave;
        yield return new WaitForSeconds(1f);
        Game.Combo.Compleete(rerank);
        yield return new WaitForSeconds(2f);
    }

    IEnumerator WaveLevelAndBoss(string level, ShipData bossData)
    {
        yield return WaveSpawnRoutine(_segments[level]);
        yield return new WaitForSeconds(3f);
        yield return SingleBoss(bossData);
    }

    IEnumerator SingleBoss(ShipData bossData)
    {
        SpawnBoss(BossPrefab, bossData, 0f, 5f);
        yield return null;
        yield return new WaitUntil(() => GameObject.FindGameObjectsWithTag("Ship").Length < 2);
    }

    ShipData[] GetDifficultyDatasWave()
    {
        switch (Game.Combo.CurrentRank)
        {
            case 1:
                return new ShipData[] { datas[1], datas[1] };
            case 2:
                return new ShipData[] { datas[1], datas[1], datas[2] };
            case 3:
                return new ShipData[] { datas[1], datas[2], datas[3] };
            case 4:
                return new ShipData[] { datas[2], datas[2], datas[3] };
            case 5:
                return new ShipData[] { datas[2], datas[2], datas[4], datas[6] };
            default:
                return new ShipData[] { datas[0], datas[0] }; //should not happen
        }
    }

    IEnumerator RandomWaves(string[] sets)
    {
        List<string> smallSegments = new List<string>();


        string[] letters = new string[] { "a", "b", "c", "d", "e", "f" };
        foreach (var s in sets)
        {
            foreach (var l in letters)
            {
                smallSegments.Add("Segment" + s + "_" + l);
            }
        }
        for (int i = 0; i < 4; i++)
        {

            yield return WaveSpawnRoutine(_segments[smallSegments[Random.Range(0, smallSegments.Count)]]);
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator WaveSpawnRoutine(WaveEnum waveType, int size, float step, ShipData data)
    {
        EnemyWave wave = new EnemyWave(size, step);
        wave.SetFunction(waveType);
        List<float> mobs = new List<float>();

        while (true)
        {
            mobs.Clear();
            if(wave.SpawnNext(ref mobs))
            {
                foreach(var x in mobs)
                {
                    SpawnEnemy(BasicPrefab, data, x);
                }
                yield return new WaitForSeconds(step);
            } else
            {
                yield break;
            }
        }
    }

    IEnumerator WaveSpawnRoutine(SegmentData segment, float step = 0.5f)
    {

        ShipData[] ships = GetDifficultyDatasWave();
        float mapRadius = Game.PlayAreaRadius;

        foreach (List<int> wave in segment.GetWaves())
        {
            int x = 0;
            foreach (var i in wave)
            {
                if (i > -1 && i<ships.Length)
                {
                    SpawnEnemy(BasicPrefab, ships[i], mapRadius + x);
                }
                x--;
            }
            yield return new WaitForSeconds(step);
        }
    }

    IEnumerator BossWaveSpawnRoutine(SegmentData segment)
    {
        ShipData[] ships = GetDifficultyDatasWave();


        List<ShipBase> mobs = new List<ShipBase>();

        float mapRadius = Game.PlayAreaRadius;
        int y = 0;

        foreach (List<int> wave in segment.GetWaves())
        {
            int x = 0;
            foreach (var i in wave)
            {
                if (i > -1 && i < ships.Length)
                {
                    ShipBase mob = SpawnBoss(BossSmallPrefab, ships[i], mapRadius + x, y);
                    mob.GetComponent<PilotBoss>().State = PilotState.Manouvering;
                    mobs.Add(mob);
                }
                x--;
            }
            y++;
            
        }
        yield return new WaitUntil(() =>GameObject.FindGameObjectsWithTag("Ship").Length < 2);
    }
}
