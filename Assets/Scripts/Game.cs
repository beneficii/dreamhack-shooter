﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {

    private static Game _instance;
    
    private static AudioSource _audioSource;
    public static AudioSource Audio
    {
        get
        {
            if(!_audioSource)
            {
                _audioSource = _instance.GetComponent<AudioSource>();
                // do some init
            }

            return _audioSource;
        }
    }

    static float _playAreaRadius = -1f;
    public static float PlayAreaRadius
    {
        get
        {
            if (_playAreaRadius < 0)
            {
                _playAreaRadius = (Camera.aspect * Camera.orthographicSize) - 0.5f;
            }
            return _playAreaRadius;
        }
    }

    private static Camera _camera;
    public static Camera Camera
    {
        get
        {
            if (!_camera)
            {
                _camera = GameObject.FindObjectOfType<Camera>();
                // do some init
            }

            return _camera;
        }
    }


    [SerializeField] private ComboCounter _comboCounter;
    public static ComboCounter Combo
    {
        get
        {
            return _instance._comboCounter;
        }
    }

    [SerializeField] private HealthBar _healthBar;
    public static HealthBar HealthBar
    {
        get
        {
            return _instance._healthBar;
        }
    }

    [SerializeField] private HealthBar _bossBar;
    public static HealthBar BossBar
    {
        get
        {
            return _instance._bossBar;
        }
    }

    // make sure it executes first in project settings
    private void Awake()
    {
        _instance = this;
    }

    public static void StartDefeatRoutine()
    {
        _instance.StartCoroutine(DelayedDefeat());
    }

    static IEnumerator DelayedDefeat()
    {
        yield return new WaitForSeconds(2f);
        PausePanel.Show(PauseState.Defeat);
    }
}
