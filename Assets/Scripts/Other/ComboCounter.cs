﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ComboCounter : MonoBehaviour {

    public Text CounterText;
    [SerializeField] HealthBar _rankBar;

    public int CurrentRank = 3;

    int _currentCombo;
    int _points;
    int _maxPossiblePoints;

    Sequence _sequence;

    private void Start()
    {
        CounterText.text = "";
        ResetCounters();
        _rankBar.Init(5);
        _rankBar.SetMax(0);
    }

    void ResetCounters()
    {
        //_currentCombo = 0;
        _points = 0;
        _maxPossiblePoints = 0;
    }

	void Add(int amount = 1)
    {
        _currentCombo += amount;
        _points += amount;
        _maxPossiblePoints += amount;
        if (_currentCombo > 6)
        {
            CounterText.text = _currentCombo.ToString() + "x";
            ShowCounter();
        }
    }

    void Break(int missed = 1)
    {
        if (_currentCombo > 6)
        {
            HideDelayed();
        }
        _currentCombo = 0;
        _maxPossiblePoints += missed;

    }


    public void ShowCounter()
    {
        _sequence.Kill();
        transform.localScale = Vector3.one;
        GetComponent<CanvasGroup>().alpha = 1f;
    }

    public void HideDelayed()
    {
        _sequence = DOTween.Sequence();
        _sequence.Append(transform.DOScale(1.4f, 0.4f).SetEase(Ease.InOutBack));
        _sequence.Append(GetComponent<CanvasGroup>().DOFade(0f, 2f).SetEase(Ease.InSine));
        _sequence.Play();
    }

    // probably put in some event script
    public void ShipDown()
    {
        Add(1);
    }

    public void ShipMissed()
    {
        Break(1);
    }

    public void Compleete(bool rerank = true)
    {
        //CounterText.text = "Points: " + _points.ToString() + "/" + _maxPossiblePoints.ToString();
        if (rerank)
        {
            int rank = 0;
            float performance = (float)_points / _maxPossiblePoints;
            if (performance > 0.7f)
            {
                rank = +1;
            }
            else if (performance < 0.4f)
            {
                rank = -1;
            }

            CurrentRank = Mathf.Clamp(CurrentRank + rank, 1, 5);

            StartCoroutine(ShowRank());
        }
        ResetCounters();
    }

    IEnumerator ShowRank()
    {
        _rankBar.SetMax(CurrentRank);
        _rankBar.transform.localScale = Vector3.one * 0.2f;
        _rankBar.transform.DOScale(1f, 0.6f).SetEase(Ease.InOutBack);
        yield return new WaitForSeconds(3f);
        _rankBar.transform.localScale = Vector3.zero;
        //_rankBar.transform.DOScale(0.2f, 0.2f).SetEase(Ease.InOutBack).onComplete = ()=> _rankBar.transform.localScale = Vector3.zero;

    }
}
