﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectSound : MonoBehaviour {

    public AudioClip audioClip;
    public float Loudness = 1f;

    void Start ()
    {
        Game.Audio.PlayOneShot(audioClip);
        Destroy(gameObject);
    }
}
