﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;



[System.Serializable]
public class SegmentData
{
    public string name;

    public int width;
    public int height;

    public string data;


    public IEnumerable<List<int>> GetWaves()
    {
        List<int> mobs = new List<int>();

        int x = 0;
        foreach(var c in data)
        {
            mobs.Add((c - '1'));

            x = (x + 1) % width;
            if (x == 0)
            {
                yield return mobs;
                mobs.Clear();
            }
        }
    }
}

public enum WaveEnum
{
    Straight,
    Random1,
    Random2,
    Random3,
    RandomSizeRandom,
    DiagonalLeft,
    DiagonalRight,
    Middle
}

public class EnemyWave
{
    const float MapLimitMin = -5;
    const float MapLimitMax = 5f;

    // gets num of spawn, returns coordinates to spawn
    System.Func<IEnumerable<float>> _enemySpawnFunc;

    int _size;
    int _index;
    float _step;

    public float y
    {
        get { return _step * _index; }
    }

    public EnemyWave(int size, float step)
    {
        _size = size;
        _index = 0;
        _step = step;
    }


    public bool SpawnNext(ref List<float> positions)
    {
        Assert.IsTrue(positions != null);

        if (_index >= _size)
        {
            return false;
        }

        foreach (var pos in _enemySpawnFunc())
        {
            positions.Add(pos);
        }
        _index++;

        return true;
    }

    System.Func<IEnumerable<float>> GetFunction(WaveEnum fType)
    {
        switch (fType)
        {
            case WaveEnum.Straight:
                float x = Random.Range(MapLimitMin, MapLimitMax);
                return () => StraightWave(x);
            case WaveEnum.Random1:
                return () => RandomWave(1);
            case WaveEnum.Random2:
                return () => RandomWave(2);
            case WaveEnum.Random3:
                return () => RandomWave(2);
            case WaveEnum.RandomSizeRandom:
                return () => RandomSizeRandomWave(3);
            case WaveEnum.DiagonalLeft:
                return () => DiagonalWave(true);
            case WaveEnum.DiagonalRight:
                return () => DiagonalWave(false);
            case WaveEnum.Middle:
                return () => StraightWave(0);
            default:
                return null;
        }
    }

    public void SetFunction(WaveEnum fType)
    {
        _enemySpawnFunc = GetFunction(fType);
    }

    // ---  Spawn Functions
    IEnumerable<float> StraightWave(float x)
    {
        yield return x;
    }

    IEnumerable<float> RandomSizeRandomWave(int maxLength)
    {
        foreach (var item in RandomWave(Random.Range(1, maxLength)))
        {
            yield return item;
        }
    }

    IEnumerable<float> RandomWave(int len)
    {
        List<float> xs = new List<float>(len);

        for (int i = 0; i < len; i++)
        {
            while (true)
            {
                float x = Random.Range(MapLimitMin, MapLimitMax);
                foreach (var xx in xs)
                {
                    // give some space between mobs
                    if (Mathf.Abs(xx - x) < _step)
                    {
                        x = -10f;
                        break;
                    }
                }
                if (x > -5f)
                {
                    xs.Add(x);
                    yield return x;
                    break;
                }
            }
        }
    }

    IEnumerable<float> DiagonalWave(bool growing)
    {
        yield return MapLimitMin + 2 * (growing ? y : MapLimitMax - y);
    }
}