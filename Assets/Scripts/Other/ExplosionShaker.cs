﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;


public class ExplosionShaker : MonoBehaviour {

	void Start () {
        CameraShaker.Instance.ShakeOnce(2f, 6f, 0.2f, 0.4f);
    }
}
