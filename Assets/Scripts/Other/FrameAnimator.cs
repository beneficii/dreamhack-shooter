﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameAnimator : MonoBehaviour {

    public AudioClip audioClip;
    public float Loudness = 1f;

    public float FrameDelay;

    int _index;

    private void OnEnable()
    {
        Game.Audio.PlayOneShot(audioClip, Loudness);
        _index = 0;
        StartCoroutine(AnimateAndDie());
    }

    IEnumerator AnimateAndDie()
    {
        
        while(_index < transform.childCount)
        {
            transform.GetChild(_index).gameObject.SetActive(true);
            yield return new WaitForSeconds(FrameDelay);
            transform.GetChild(_index).gameObject.SetActive(false);
            _index++;
        }
        Destroy(gameObject);
        //gameObject.SetActive(false);
    }
}
