﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tilter : MonoBehaviour {

    public float Tilt;

    Rigidbody2D _rb2D;

	// Use this for initialization
	void Start () {
        _rb2D = GetComponentInParent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.localRotation = Quaternion.Euler(Vector3.down*_rb2D.velocity.x * Tilt);
    }
}
