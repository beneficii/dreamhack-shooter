﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum PilotState
{
    GoingForMiddle,
    Manouvering
}

public class AutoPilotBase : MonoBehaviour {

    public float Speed = 2f;

    protected Rigidbody2D _rb2D;

    private void Awake()
    {
        _rb2D = GetComponent<Rigidbody2D>();
    }
}
