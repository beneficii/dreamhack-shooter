﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PilotBoss : AutoPilotBase {

    const float ArriveSpeed = 2.5f;

    public float SwingRadius = 4f;

    static int _index = 0;
    List<Vector2> _directions;
    protected Vector2 _middlePosition;
    public PilotState State;


    public void Init(Vector2 pos)
    {
        _middlePosition = pos;
        _directions = new List<Vector2>(2)
        {
            Vector2.left,
            Vector2.right
        };
        State = PilotState.GoingForMiddle;
    }


    private void FixedUpdate()
    {
        if (State == PilotState.GoingForMiddle)
        {
            _rb2D.position = Vector2.MoveTowards(_rb2D.position, _middlePosition, ArriveSpeed * Time.deltaTime);
            if (_rb2D.position == _middlePosition)
            {
                State = PilotState.Manouvering;
            }
        }
        else
        {
            _rb2D.position += _directions[_index] * Speed * Time.deltaTime;
            if (_rb2D.position.x * _directions[_index].x > SwingRadius)
            {
                //_index = 1;
                _index = (_index + 1) % _directions.Count;
            }
        }
    }
}
