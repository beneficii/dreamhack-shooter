﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilotPlayer : AutoPilotBase {

    Boundary boundary;

    float _moveHorizontal;
    float _moveVertical;


    // Use this for initialization
    void Start () {
        boundary = new Boundary(0.7f);
        boundary.yMax -= 1f;
        boundary.yMin += 1f;
    }
	
	// Update is called once per frame
	void Update () {
        _moveHorizontal = Input.GetAxis("Horizontal");
        _moveVertical = Input.GetAxis("Vertical");
    }

    private void FixedUpdate()
    {
        _rb2D.velocity = new Vector3(_moveHorizontal, _moveVertical) * Speed;

        _rb2D.position = new Vector3
        (
            Mathf.Clamp(_rb2D.position.x, boundary.xMin, boundary.xMax),
            Mathf.Clamp(_rb2D.position.y, boundary.yMin, boundary.yMax)
        );
    }
}
