﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilotStraight : AutoPilotBase {

    public Vector2 Direction;

	void Start () {
        _rb2D.velocity = Direction * Speed;
        Destroy(this); // to improve performance
	}
}
