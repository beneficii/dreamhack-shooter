﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShipBase : MonoBehaviour {

    public BulletData CurrentBullet;

    public Transform BlasterTransform;
    public GameObject ExplosionPrefab;

    protected bool _isShooting = false;

    protected ShipData _data;


    protected HealthBar _hpBar;
    protected int _health;
    public int Health
    {
        get { return _health; }
        set
        {
            _health = Mathf.Max(value, 0);//Mathf.Clamp(value, 0, _data.Health);
            if(_hpBar != null)
            {
                _hpBar.UpdateHP(_health);
            }

            if(_health == 0)
            {
                Destroy();
            }
        }
    }

    protected float _lastFire;
    protected Rigidbody2D _rb2D;

    public virtual void Init(ShipData data)
    {
        _data = data;
        Health = _data.Health;
        _lastFire = Time.time;
        _rb2D = GetComponent<Rigidbody2D>();
        Instantiate(_data.Mesh, transform);
    }

    public virtual void Destroy()
    {
        if (ExplosionPrefab != null)
        {
            Instantiate(ExplosionPrefab, transform.position, transform.rotation);
        }
        Destroy(gameObject);
    }

    protected bool CanShoot
    {
        get { return  CurrentBullet != null && _lastFire + _data.Firerate <= Time.time; }
    }

    public virtual BulletBase Shoot(BulletData bulletData)
    {
        BulletBase bullet = bulletData.Create();
        bullet.transform.position = BlasterTransform.position;
        _lastFire = Time.time;
        return bullet;
    }

    private void FixedUpdate()
    {
        if (_isShooting && CanShoot)
        {
            Shoot(CurrentBullet);
        }
    }
}
