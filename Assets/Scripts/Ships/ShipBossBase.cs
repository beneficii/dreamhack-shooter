﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBossBase : ShipBase {

    [Header("Override data stats")]
    public int MaxHealth = 20;

    public override void Init(ShipData data)
    {
        base.Init(data);
        Health = MaxHealth;
        _isShooting = _data.Firerate > 0f;
        _hpBar = Game.BossBar;
        _hpBar.Init(Health);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Ship"))
        {
            ShipPlayer player = other.GetComponent<ShipPlayer>();
            Debug.Log("Ship enetered " + other.name);

            if (player != null)
            {
                player.Health -= 100;
            }
        }
    }

    public override BulletBase Shoot(BulletData bulletData)
    {
        var bullet = base.Shoot(bulletData);
        bullet.SetDirection(Vector3.down);
        bullet.gameObject.layer = LayerMask.NameToLayer("Enemies");
        bullet.transform.localScale = transform.localScale;
        return bullet;
    }
}
