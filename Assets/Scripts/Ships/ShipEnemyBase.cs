﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipEnemyBase : ShipBase {


    Boundary _limitingBoundary;

    public override void Init(ShipData data)
    {
        base.Init(data);
        _limitingBoundary = new Boundary(-4f);
        _isShooting = _data.Firerate > 0f;
    }


    void Update()
    {
        if (_limitingBoundary.OutOfBounds(_rb2D.position))
        {
            Game.Combo.ShipMissed();
            Destroy(gameObject);
        }
    }

    public override void Destroy()
    {
        base.Destroy();
        Game.Combo.ShipDown();
    }


    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Ship"))
        {
            ShipPlayer player = other.GetComponent<ShipPlayer>();
            if (player != null)
            {
                _data.OnCollision(player);
                Destroy();
            }
        }
    }

    public override BulletBase Shoot(BulletData bulletData)
    {
        var bullet = base.Shoot(bulletData);
        bullet.SetDirection(Vector3.down);
        bullet.gameObject.layer = LayerMask.NameToLayer("Enemies");
        return bullet;
    }
}
