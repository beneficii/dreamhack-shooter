﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipPlayer : ShipBase {

    public ShipData InitialData;


    private void Awake()
    {
        Init(InitialData);
    }

    public override void Init(ShipData data)
    {
        base.Init(data);
        _hpBar = Game.HealthBar;
        _hpBar.Init(Health);
    }

    public override BulletBase Shoot(BulletData bulletData)
    {
        var bullet = base.Shoot(bulletData);
        bullet.SetDirection(Vector3.up);
        bullet.gameObject.layer = LayerMask.NameToLayer("Player");
        return bullet;
    }

    protected void Update()
    {
        if(Input.GetButtonDown("Jump"))
        {
            _isShooting = !_isShooting;
        }

        if(Input.GetKeyDown(KeyCode.Q))
        {
            Health = 0;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            //Health++;
        }
    }

    public override void Destroy()
    {
        base.Destroy();
        Game.StartDefeatRoutine();
    }
}
