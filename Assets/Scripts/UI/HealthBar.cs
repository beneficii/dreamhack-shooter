﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

    public GameObject heartPrefab;
    List<Heart> hearts;

    public void Init(int hp, int startingHP = -1)
    {
        if(startingHP < 0)
        {
            startingHP = hp;
        }
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        hearts = new List<Heart>(hp);
        for (int i = 0; i < hp; i++)
        {
            Heart heart = Instantiate(heartPrefab, transform).GetComponentInChildren<Heart>();
            if(i>=startingHP)
            {
                heart.OutInstant();
            }
            hearts.Add(heart);
        }
    }

    public void UpdateHP(int hp)
    {
        for (int i = 0; i < hearts.Count; i++)
        {
            hearts[i].Working = i < hp;
        }
    }

    public void SetMax(int hp)
    {
        for (int i = 0; i < hearts.Count; i++)
        {
            hearts[i].transform.parent.gameObject.SetActive(i < hp);
        }
    }
}
