﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Heart : MonoBehaviour {

    bool _working = true;
    public bool Working
    {
        set
        {
            if(value != _working)
            {
                if(value)
                {
                    AnimateIn();
                } else
                {
                    AnimateOut();
                }

                _working = value;
            }

        }
    }

    CanvasGroup _cg;
    Sequence _sequence;

    private void Awake()
    {
        _cg = GetComponent<CanvasGroup>();
    }

    void AnimateIn()
    {
        transform.localScale = Vector3.one*0.2f;
        transform.DOScale(1f, 0.3f).SetEase(Ease.InOutBack);
        _cg.DOFade(1f, 0.2f).SetEase(Ease.InSine);

    }

    void AnimateOut()
    {
        transform.localScale = Vector3.one;

        _sequence = DOTween.Sequence();
        _sequence.Append(transform.DOScale(1.2f, 0.2f).SetEase(Ease.InOutBack));
        _sequence.Append(GetComponent<CanvasGroup>().DOFade(0f, 0.2f).SetEase(Ease.InSine));
        _sequence.Play();
    }

    public void OutInstant()
    {
        GetComponent<CanvasGroup>().alpha = 0f;
    }
}
