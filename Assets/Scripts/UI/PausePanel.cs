﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PauseState
{
    Paused,
    Victory,
    Defeat
}

public class PausePanel : MonoBehaviour {

    public Text Title;
    public Text Body;
    public HealthBar Rank;
    public HealthBar BestRank;


    static PausePanel _instance;

    const string scoreID = "Score_RankV2";
    public int ScoreBest
    {
        get { return PlayerPrefs.GetInt(scoreID, 0); }
        set
        {

            if (value > ScoreBest)
            {
                PlayerPrefs.SetInt(scoreID, value);
            }
        }
    }

    private void Awake()
    {
        Time.timeScale = 0f;
        _instance = this;
        BestRank.Init(5, ScoreBest);
    }

    public static void Show(PauseState state)
    {
        Time.timeScale = 0f;
        _instance.ShowPanel(state);
    }

    void ShowPanel(PauseState state)
    {
        switch (state)
        {
            case PauseState.Paused:
                Title.text = "Paused";
                Body.text = "";
                break;
            case PauseState.Victory:
                Title.text = "Victory!";
                Body.text = "Press 'R' to restart!\n(Game changes a bit depending on your Rank)";
                break;
            case PauseState.Defeat:
                Title.text = "Defeat!";
                Body.text = "Press 'R' to restart!";
                break;
            default:
                break;
        }
        if(state == PauseState.Victory)
        {
            Rank.gameObject.SetActive(true);
            Rank.Init(Game.Combo.CurrentRank);
            int score = Game.Combo.CurrentRank;
            if (score > ScoreBest) {
                ScoreBest = score;
                BestRank.Init(5, score);
            }

        } else
        {
            Rank.gameObject.SetActive(false);
        }
        gameObject.SetActive(true);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Hide();
        }
    }

    void Hide()
    {
        Time.timeScale = 1f;
        gameObject.SetActive(false);
    }
}
