﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnScreenTimer : MonoBehaviour {

    Text UIDisplay;

	void Start () {
        UIDisplay = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        UIDisplay.text = Mathf.RoundToInt(Time.timeSinceLevelLoad).ToString();
	}
}
