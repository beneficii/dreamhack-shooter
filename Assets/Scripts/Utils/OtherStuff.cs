﻿using UnityEngine;

public class Boundary
{
    public float xMin, xMax, yMin, yMax;

    public Boundary()
    {
        Camera cam = Game.Camera;
        float halfWidth = cam.orthographicSize * cam.aspect;
        xMin = cam.transform.position.x - halfWidth;
        xMax = cam.transform.position.x + halfWidth;
        yMin = cam.transform.position.y - cam.orthographicSize;
        yMax = cam.transform.position.y + cam.orthographicSize;
    }

    public Boundary(float padding)
    {
        Camera cam = Game.Camera;
        float halfWidth = cam.orthographicSize * cam.aspect;
        xMin = cam.transform.position.x - halfWidth + padding;
        xMax = cam.transform.position.x + halfWidth - padding;
        yMin = cam.transform.position.y - cam.orthographicSize + padding;
        yMax = cam.transform.position.y + cam.orthographicSize - padding;
    }

    public bool OutOfBounds(Vector2 point)
    {
        return xMin > point.x || point.x > xMax || yMin > point.y || point.y > yMax;
    }
}