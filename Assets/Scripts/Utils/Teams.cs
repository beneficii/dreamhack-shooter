﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Team
{
    Blue,
    Red,
    Yellow // maybe for future
}

static class TeamUtils
{
    public static Vector3 GetDirection(this Team team)
    {
        switch (team)
        {
            case Team.Blue:
                return Vector3.up;
            case Team.Red:
                return Vector3.down;
            default:
                Debug.LogError("Wrong team");
                return Vector3.zero;
        }
    }
}
