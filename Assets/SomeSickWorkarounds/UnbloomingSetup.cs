﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnbloomingSetup : MonoBehaviour {

    public Camera MyCam;

	void Start () {
        

        transform.localScale = new Vector3(MyCam.orthographicSize * MyCam.aspect*2, MyCam.orthographicSize * 2f, 1f);

        RenderTexture texture = new RenderTexture((int)(transform.localScale.x*10), (int)(transform.localScale.y*10), 16, RenderTextureFormat.ARGB64);
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = texture;

        //transform.position = MyCam.transform.position;
        MyCam.targetTexture = texture;

    }
}
